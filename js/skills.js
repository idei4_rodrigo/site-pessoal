(function($){
	var conhecimentos = $(".classeConhecimento");
	var url = "http://rdfreitas.com.br/skills.json";


	$.getJSON(url, function(retorno){
		var ul = $("<ul>");


		$.each(retorno.skill, function(){

			var li = $("<li>");
			var sNome = $("<span class='quantidade-conhecimento'>").text(this.nome);
			var sQuantidade = $("<span class='qual-conhecimento'>").text(this.quantidade);

			li.append(sNome).append(sQuantidade).appendTo(ul);
		});

		$("ul", conhecimentos).remove();
		ul.appendTo(conhecimentos);
	});
}) (jQuery);