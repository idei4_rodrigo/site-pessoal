var App = angular.module('App', ['ui.router']);
App.config(function($stateProvider){
    $stateProvider
        .state('index', {
            url: "",
            views:{
                "perfil"            : {templateUrl: "pages/perfil.html"},
                "ano-2015"          : {templateUrl: "pages/ano2015.html"},
                "ano-2014"          : {templateUrl: "pages/ano2014.html"},
                "ano-2013"          : {templateUrl: "pages/ano2013.html"},
                "ano-2012"          : {templateUrl: "pages/ano2012.html"},
                "ano-2010"          : {templateUrl: "pages/ano2010.html"},
                "ano-2006"          : {templateUrl: "pages/ano2006.html"},
                "ano-2005"          : {templateUrl: "pages/ano2005.html"},
                "conhecimentos"     : {templateUrl: "pages/conhecimento.html"},
                "trabalhos"         : {templateUrl: "pages/portfolio.html"}
                
            }
        })
        .state('english', {
            url: "/english",
            views:{
                "perfil": {templateUrl: "pages/english-perfil.html"},
                "ano-2014": {templateUrl: "pages/english-ano2014.html"},
                "ano-2013": {templateUrl: "pages/english-ano2013.html"},
                "ano-2012": {templateUrl: "pages/english-ano2012.html"},
                "ano-2010": {templateUrl: "pages/english-ano2010.html"},
                "ano-2006": {templateUrl: "pages/english-ano2006.html"},
                "ano-2005": {templateUrl: "pages/english-ano2005.html"}
            }
        })
});
